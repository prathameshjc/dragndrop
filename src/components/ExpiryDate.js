import React from "react";

export default class ExpiryDate extends React.Component {
  state = {
    date: ""
  };

  componentDidMount() {
    this.getDate();
  }

  getDate = () => {
      var d=new Date();
    var year = d.getFullYear();
    var month = d.getMonth();
    var day = d.getDate();
    var date = new Date(year + 1, month, day).toDateString();
    this.setState({ date });
  };

  render() {
    const { date } = this.state;

    return <div> Expiry Date : {date}</div>;
  }
}