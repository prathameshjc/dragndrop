import React from 'react';
import { Card, CardHeader, CardBody, Input } from 'reactstrap';
import Barcode from 'react-barcode';
import Image from 'react-random-image';
import QRCode from 'qrcode.react';
// import ExpiryDateInput from "credit-card-expiry-date";
import Random from './RandomNumber';
import ExpiryDate from './ExpiryDate';
import SourceCode from './SourceCode';
// import ExpirationDate from './ExpirationDate';
// import ExpiryDate from './ExpirationDate.Js';
// import ExpirationDate from './ExpirationDate.Js';
// import genetrateCode from './SourceCode';
// var Barcode = require('react-barcode');
function palette() {
    return (
        <div>
            <div className="row mt-5 mr-md-0 ml-md-0">
                <div className="card-group col-sm-8">
                    <Card>
                        <CardHeader className="font-weight-bold">Drawing Palette</CardHeader>
                        <CardBody className=" container">
                            <textarea rows={11} className="col-md" name="text" id="exampleText" />
                        </CardBody>
                    </Card>
                </div>
                <div className="col-sm-4 card-group">
                    <Card>
                        <CardHeader className="font-weight-bold">Design Objects Palette</CardHeader>
                        <CardBody className="draggable row" draggable>
                            <ul className="list-unstyled list-group row-content col-md">
                                <li className="list-group-item">
                                    <Image width={100} height={100} /></li>
                                <li className="list-group-item">
                                    <label className="border-dark">Product Name</label><br />
                                </li>
                                <li className="list-group-item"><QRCode value="http://facebook.github.io/react/" size={50} /></li>
                            </ul>
                            <ul className="list-unstyled list-group row-content col-md">
                                <li className="list-group-item"><Random /></li>
                                <li className="list-group-item"><SourceCode /></li>
                                <li className="list-group-item"><ExpiryDate /></li>
                                <li className="list-group-item"><Barcode value="1234567890" width={1} height={25} /></li>
                                {/* <li className="rounded"><QRCode value="http://facebook.github.io/react/" size={50}/></li> */}
                            </ul>
                            {/* <img src="/images/prathamesh.jpg" class="img-rounded" alt=""/> */}
                            {/* <Barcode value="1234567890"/> */}
                        </CardBody>
                    </Card>
                </div>
            </div>
        </div>
    )
}

export default palette;