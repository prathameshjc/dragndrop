import React, { Component } from 'react'
import Palette from './components/Palette'

class App extends Component {
  render() {
    return (
      <div>
        <Palette/>
      </div>
    )
  }
}

export default App
